# Project Builder Web App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.2.

## Development

### Server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running lint

Run `ng lint` to execute the lint control with eslint.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via cypress.

## Execute production Image

docker run -it -p 80:80 registry.gitlab.com/aegis-techno/software/project-builder/web-app:latest -d
