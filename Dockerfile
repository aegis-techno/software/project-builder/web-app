FROM  nginx:1.21.0

LABEL authors="Aegis <boris.bodin@gmail.com>"
LABEL vendor="Aegis-Techno"
LABEL tools="project-builder"

# Copy files
COPY dist/web-app /usr/share/nginx/html
